
fu! Act_file_exit()
    if &mod
        let question = "This file is not preserved, do you still have to quit?"
        let choices = "&Yes\n&No"
        let choice = quickui#confirm#open(question, choices, 1, "Quit")

        if choice == 1
            execute('qa!')
        endif
    else
        execute('qa')
    endif 
endf

fu! quick#view#actions_file#init()
    call quickui#menu#install("&File", [
            \ ["E&xit", 'call Act_file_exit()'],
          \ ])
endf
