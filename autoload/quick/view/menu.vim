
fu! quick#view#menu#init_view()
    call quickui#menu#install("&View", [
            \ ["&File Explorer", "QuickToggleExplorer"],
            \ ["&Terminal", "QuickToggleTerminal"],
            \ ["&Encoding", "QuickToggleEncoding"],
            \ ["T&ags", "QuickToggleTags"],
          \ ])
endf

fu! quick#view#menu#init()
    " vim-quickui
    call quickui#menu#reset()

    call quick#view#actions_file#init()
    call quick#view#actions_view#init()
    call quick#view#actions_help#init()
endf
