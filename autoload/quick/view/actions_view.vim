
fu! quick#view#actions_view#init()
    call quickui#menu#install("&View", [
            \ ["&File Explorer", "QuickToggleExplorer"],
            \ ["&Terminal", "QuickToggleTerminal"],
            \ ["&Encoding", "QuickToggleEncoding"],
            \ ["T&ags", "QuickToggleTags"],
            \ ["&Buffer", "call quickui#tools#list_buffer('e')"],
            \ ["--"],
            \ ["&Wrap %{&wrap ? 'on' : 'off'}", "set wrap!"],
            \ ["&Line number %{&number ? 'on' : 'off'}", "set nu!"],
            \ ["&Relative number %{&relativenumber ? 'on' : 'off'}", "set relativenumber!"],
          \ ])
endf
