
fu! Act_about_vim_quick()
    let content = []

    call add(content, '')
    call add(content, 'Commands: ')
    call add(content, "\tQuickToggleExplorer    toggle file explorer window")
    call add(content, "\tQuickToggleTerminal    toggle terminal window")
    call add(content, "\tQuickToggleEncoding    toggle encoding window")
    call add(content, "\tQuickToggleTags        toggle tags window")
    call add(content, '--------------------------------------------')
    call add(content, 'Hotkeys: ')
    call add(content, "\t<SPACE><SPACE>         open the menubar")
    call add(content, "\t<SPACE>vf              :QuickToggleExplorer")
    call add(content, "\t<SPACE>vt              :QuickToggleTerminal")
    call add(content, "\t<SPACE>ve              :QuickToggleEncoding")
    call add(content, "\t<SPACE>va              :QuickToggleTags")
    call add(content, '--------------------------------------------')
    call add(content, 'Coc: ')
    call add(content, "\t<C-Tab>                next pop item")
    call add(content, "\t<S-Tab>                previous pop item")
    call add(content, "\t<CR>                   check pop item")
    call add(content, "\tK                      show documentation")
    call add(content, "\tgd                     go to defineition")
    call add(content, "\tgy                     go to type defineition")
    call add(content, "\tgi                     go to implementation")
    call add(content, "\tgr                     go to references")
    call add(content, "\t<Leader>f              coc format selected")
    call add(content, "\t<Leader>a              coc codeaction selected")
    call add(content, "\t<Leader>ac             coc codeaction")
    call add(content, "\t<Leader>qf             coc quickfix")
    call add(content, "\t<Leader>ccl             coc code lens")
    
    let opts = {"close": "button", "title": "About vim-quick", "w": 80, "h": 20}
    call quickui#textbox#open(content, opts)
endf

fu! quick#view#actions_help#init()
    call quickui#menu#install("&Help", [
            \ ["About vim-&quick", 'call Act_about_vim_quick()'],
          \ ])
endf
