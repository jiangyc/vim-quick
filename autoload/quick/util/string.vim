
fu! quick#util#string#is_empty(str)
    return len(a:str) <= 0
endf

fu! quick#util#string#is_blank(str)
    " 空串，肯定属于空白字符
    if quick#util#string#is_empty(a:str)
        return 1
    endif
    " 判断空白串逻辑
    return len(split(a:str, '\W\+')) <= 0
endf

fu! quick#util#string#concat(...)
    if a:0 <= 0
        return ''
    endif

    return join(a:000, '')
endf

fu! quick#util#string#trim(str)
    if quick#util#string#is_blank(a:str)
        return ''
    endif

    " 截取首部
    let idx = 0
    for c in split(a:str, '\zs')
        if !quick#util#string#is_blank(c)
            break
        endif
        let idx = idx + 1
    endfor
    let str_trim = strpart(a:str, idx)

    " 截取尾部分
    " let idx = strlen(str_trim) - 1
    let str_split = split(a:str, '\zs')
    let idx = len(str_split) - 1
    while idx >= 0
        let c = str_split[idx]
        if !quick#util#string#is_blank(c)
            break
        endif

        let idx = idx - 1
    endwhile

    return strpart(str_trim, 0, idx)
endf
