

fu! quick#util#path#separator()
    retu has('win32') || has('win64') ? '\' : '/'
endf

fu! quick#util#path#join(...)
    return a:0 <= 0 ? '' : join(a:000, quick#util#path#separator())
endf

fu! quick#util#path#parent(path)
    " find / or \
    let chars = split(a:path, '\zs')
    if len(chars) <= 0
        return
    endif
    let idx = len(chars) - 1
    while idx >= 0
        if (chars[idx] == '/' || chars[idx] == '\') && (idx < len(chars) - 1)
            break
        endif

        let idx = idx - 1
    endwhile

    " split
    if idx < 0 || idx > len(chars) - 1
        return ''
    endif

    let sub_chars = idx == 0 ? chars[0:0] : chars[0:idx - 1]
    return join(sub_chars, '')
endf
