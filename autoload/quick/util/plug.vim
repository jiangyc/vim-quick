
fu! quick#util#plug#has_plug(plug_name)
    if !exists('g:loaded_plug') || !exists('g:plug_home') || !exists('g:plugs_order')
        return 0
    endif
    
    let plug_declared = 0
    for plug in g:plugs_order
        if a:plug_name == plug
            let plug_declared = 1
        endif
    endfor
    if !plug_declared
        return 0
    endif

    let plug_path = g:plug_home . quick#util#path#separator() . a:plug_name
    return isdirectory(plug_path)
endf

fu! quick#util#plug#add_hook(plug_name, func_name)
    if !exists('g:quick_plug_hooks')
        let g:quick_plug_hooks = {}
    endif

    if !quick#util#plug#has_plug(a:plug_name)
        return
    endif

    if !has_key(g:quick_plug_hooks, a:plug_name)
        let g:quick_plug_hooks[a:plug_name] = []
    endif

    call add(g:quick_plug_hooks[a:plug_name], a:func_name)
endf

fu! quick#util#plug#do_hook()
    if !exists('g:quick_plug_hooks')
        return
    endif

    for plug_name in keys(g:quick_plug_hooks)
        for func_name in g:quick_plug_hooks[plug_name]
            let Func = function(func_name)
            call Func()
        endfor
    endfor
endf
