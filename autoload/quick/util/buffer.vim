
fu! quick#util#buffer#is_changed()
    " getbufvar('%', '&mod')
    " getbufvar('n' + 0, '&mod')
    retu &mod
endf

fu! quick#util#buffer#curr_file()
    retu expand('%:p')  
endf

fu! quick#util#buffer#curr_file_name()
    retu expand('%:p:t')
endf

fu! quick#util#buffer#curr_file_dir()
    retu expand('%:p:h')
endf

fu! quick#util#buffer#curr_file_ext()
    retu expand('%:p:e')
endf

fu! quick#util#buffer#list()
    redir => str_buffers
    silent! ls
    redir END

    let str_buffers = quick#util#string#trim(str_buffers)
    let buffer_list = []
    for curr in split(str_buffers, '\n')
        let buffer = {}
        " id
        let buffer.id = str2nr(matchstr(curr, '^\s*\zs\d\+'))
        " bufnr
        let buffer.bufnr = buffer.id + 0
        " file
        let buffer.file = expand('#' . buffer.id . ':p')
        " file dir
        let buffer.file_dir = expand('#' . buffer.id . ':p:h')
        " file name
        let buffer.file_name = expand('#' . buffer.id . ':p:t')
        " file ext
        let buffer.file_ext = expand('#' . buffer.id . ':p:e')
        " changed
        let buffer.changed =  getbufvar(buffer.id + 0, '&mod')

        call add(buffer_list, buffer)
    endfor

    retu buffer_list
endf
