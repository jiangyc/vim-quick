
fu! quick#widgets#terminal#open()
    let cmd = $SHELL

    if quick#util#string#is_blank(cmd)
        if !has('win32') && !has('win64')
            return
        endif

        if executable('pwsh')
            let cmd = 'pwsh'
        elseif executable('powershell')
            let cmd = 'powershell'
        elseif executable('cmd')
            let cmd = 'cmd'
        endif
    endif

    let opts = {'w': 80, 'h': 20, 'title': 'Terminal'}
    call quickui#terminal#open(cmd, opts)
endf
