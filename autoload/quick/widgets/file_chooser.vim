
if !exists('g:quick_nerd_font')
    let g:quick_nerd_font = 1
endif
if !exists('g:file_chooser_icon_dir')
    let g:file_chooser_icon_dir  = g:quick_nerd_font ? "  " : "[d]"
endif
if !exists('g:file_chooser_icon_file')
    let g:file_chooser_icon_file  = g:quick_nerd_font ? " 🗎 " : "[f]"
endif
if !exists('g:file_chooser_icon_none')
    let g:file_chooser_icon_none  = "   "
endif
let s:file_chooser_opts = {'w': 80, 'title': 'File Chooser'}

fu! quick#widgets#file_chooser#(...)
    " cwd
    let cwd = a:0 > 0 && isdirectory(a:1) ? a:1 : getcwd() 

    " items
    let items = []
    let parent_dir = quick#util#path#parent(cwd)
    if !quick#util#string#is_blank(parent_dir)
        call add(items, g:file_chooser_icon_dir . "\t..")
    endif
    let subfiles = readdir(cwd)
    for subfile in subfiles
        let subfile_path = cwd . quick#util#path#separator() . subfile

        let item = subfile
        if isdirectory(subfile_path)
            let item = g:file_chooser_icon_dir . "\t" . item
        elseif filereadable(subfile_path)
            let item = g:file_chooser_icon_file . "\t" . item
        else
            let item = "   \t" . item
        endif

        call add(items, item)
    endfor

    " events
    " m -> Context Menu
    " a -> Add file
    " A -> Add folder
    " d -> Delete
    let hwnd = quickui#listbox#open(items, s:file_chooser_opts)
    " execute hwnd.winid . ' wincmd w'
    " nnoremap <buffer> m       :echo 1<CR>
endf

" choose file
"
" param - root dir
"
" return choose file
"
fu! quick#widgets#file_chooser#open(...)
    " root dir
    let root_dir = a:0 <= 0 ? getcwd() : (a:0 == 1 ? a:1 : join(a:000, quick#util#path#separator()))
    if !isdirectory(root_dir)
        let root_dir = getcwd()
    endif

    " list dir
    let dirs = readdir(root_dir)

    " dir to show
    let dirs_show = []
    let file_type_dir  = g:quick_nerd_font ? "  " : "[d]"
    let file_type_file = g:quick_nerd_font ? " 🗎 " : "[f]"
    let file_type_none = "   "
    " parent dir
    let parent_dir = quick#util#path#parent(root_dir)
    if len(parent_dir) > 0
        let dirs_show = add(dirs_show, file_type_dir . "\t..")
    endif

    for dir in dirs
        let dir_full = root_dir . quick#util#path#separator() . dir

        if isdirectory(dir_full)
            let dir = file_type_dir . "\t" . dir
        elseif filereadable(dir_full)
            let dir = file_type_file . "\t" . dir
        else
            let dir = "   \t" . dir
        endif

        let dirs_show = add(dirs_show, dir)
    endfor

    " show dialog
    let opts = {
            \ 'title': 'Open File',
            \ 'w': 80,
          \ }
    let idx = quickui#listbox#inputlist(dirs_show, opts)
    if idx < 0
        return ''
    elseif idx == 0 && len(parent_dir) > 0
            " go parent
        return quick#widgets#file_chooser#open(parent_dir)
    endif

    let choice = root_dir . quick#util#path#separator() . (len(parent_dir) > 0 ? dirs[idx - 1] : dirs[idx])
    if isdirectory(choice)
        return quick#widgets#file_chooser#open(choice)
    endif

    return filereadable(choice) ? choice : ''
endf
