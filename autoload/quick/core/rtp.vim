
fu! quick#core#rtp#current()
    return g:rtp 
endf

fu! quick#core#rtp#has_file(...)
    let subfile = join(a:000, quick#util#path#separator())    
    for rtp in split(&rtp, ",")
        let file_full_path = rtp . quick#util#path#separator() . subfile
        if filereadable(file_full_path)
            return 1
        endif
    endfor
    return 0
endf
