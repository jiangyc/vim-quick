
fu! quick#core#vim#source(file)
    if filereadable(file)
        execute('source ' . a:file)
    endif
endf

fu! quick#core#vim#source_rtp(...)
    if a:0 <= 0
        return
    endif
    let subpath = join(a:000, quick#util#path#separator())
    let path = g:rtp . quick#util#path#separator() . subpath
    if filereadable(path)
        execute('source ' . path)
    endif
endf
