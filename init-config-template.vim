
if exists('g:loaded_init_config')
    finish
endif
let g:loaded_init_config = 1

" ======================
"     CoC
" ======================
let g:quick_coc_config = {}

" ======================
"     CoC: LSP
" ======================
let g:quick_coc_config['lsp'] = []
fu! s:QuickCocLsp(syntax, extensions)
    if quick#util#string#is_blank(a:syntax) || quick#util#string#is_blank(a:extensions)
        return
    endif
    let quick_coc_config_lsp = {}
    let quick_coc_config_lsp['syntax'] = split(a:syntax, ',')
    let quick_coc_config_lsp['extensions'] = split(a:extensions, ',')
    call add(g:quick_coc_config['lsp'], quick_coc_config_lsp)
endf

" vim
call s:QuickCocLsp('vim', 'coc-vimlsp')

" json
call s:QuickCocLsp('json', 'coc-json')

"" html
call s:QuickCocLsp('html', 'coc-html')
"" css
call s:QuickCocLsp('css', 'coc-css')
"" javascript,typescript
call s:QuickCocLsp('javascript,typescript', 'coc-tsserver')
"" vue
" call s:QuickCocLsp('vue', '@yaegassy/coc-volar-tools,@yaegassy/coc-volar')

"" cpp
" call s:QuickCocLsp('c,h,cpp', 'coc-clangd')

"" rust
" call s:QuickCocLsp('rust', 'coc-rust-analyzer')

"" java
" call s:QuickCocLsp('java', 'coc-java')

"" lua 
" call s:QuickCocLsp('lua', 'coc-lua')

"" sh
" call s:QuickCocLsp('sh', 'coc-sh')

"" ps1
" call s:QuickCocLsp('ps1', 'coc-powershell')
