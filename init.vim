
" ====================
" init.vim
"
" - setup rtp
" - load layer
" ====================

" setup rtp
let g:rtp = expand('<sfile>:p:h')
fu! Init_rtp(curr_rtp)
    let compare_rtp = join(split(g:rtp, '\'), '/')
    let compare_global_rtp = join(split(&rtp, '\'), '/')
    if stridx(compare_global_rtp, compare_rtp) < 0
        execute('set rtp+=' . g:rtp)
    endif
endf
call Init_rtp(g:rtp)

" user configurations
fu! Init_config()
    if filereadable(quick#util#path#join(g:rtp, 'init-config.vim'))
        execute('source ' . quick#util#path#join(g:rtp, 'init-config.vim'))
    else
        execute('source ' . quick#util#path#join(g:rtp, 'init-config-template.vim'))
    endif
endf
call Init_config()

" load layer
fu! Init_layer(...)
    if a:0 == 1
        let path = a:1
    elseif a:0 > 1
        let path = join(a:000, quick#util#path#separator())
    else
        let path = quick#util#path#join(g:rtp, 'layer')
    endif

    if filereadable(expand(path))
        execute('source ' . path)
    elseif isdirectory(path)
        let subfiles = readdir(path)
        for child in subfiles
            let childpath = quick#util#path#join(path, child)
            if filereadable(expand(childpath))
                execute('source ' . childpath)
            endif
        endfor
        for child in readdir(path)
            let childpath = quick#util#path#join(path, child)
            if isdirectory(expand(childpath))
                call Init_layer(childpath)
            endif
        endfor
    endif
endf
call plug#begin()
call Init_layer()
call plug#end()
call quick#util#plug#do_hook()
