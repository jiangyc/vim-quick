# vim-quick

![screenshot](images/screenshot.png)

## Install

```bash
git clone https://gitlab.com/jiangyc/vim-quick.git ~/.vim
```

## 说明

- [ ] 双击空格：菜单栏

- [x] <SPACE>vf: 文件

- [x] <SPACE>ve: 编码

- [x] <SPACE>vt: 终端
