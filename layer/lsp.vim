
if !exists('g:quick_lsp_vim_enabled')
    let g:quick_lsp_vim_enabled = 1
endif

if !exists('g:quick_lsp_json_enabled')
    let g:quick_lsp_json_enabled = 1
endif

if !exists('g:quick_lsp_html_enabled')
    let g:quick_lsp_html_enabled = 0
endif

if !exists('g:quick_lsp_css_enabled')
    let g:quick_lsp_css_enabled = 0
endif

if !exists('g:quick_lsp_js_enabled')
    let g:quick_lsp_js_enabled = 0
endif

if !exists('g:quick_lsp_vue_enabled')
    let g:quick_lsp_vue_enabled = 0
endif

if !exists('g:quick_lsp_cpp_enabled')
    let g:quick_lsp_cpp_enabled = 0
endif

if !exists('g:quick_lsp_rust_enabled')
    let g:quick_lsp_rust_enabled = 0
endif

if !exists('g:quick_lsp_java_enabled')
    let g:quick_lsp_java_enabled = 0
endif

