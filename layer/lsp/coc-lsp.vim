
if g:quick_coc_enabled != 1 || !exists('g:quick_coc_config') || !quick#util#plug#has_plug('coc.nvim')
    finish
endif

if !exists('g:coc_global_extensions')
    let g:coc_global_extensions = []
endif

for lsp in g:quick_coc_config['lsp']
    for extension in lsp['extensions']
        call add(g:coc_global_extensions, extension)
    endfor
endfor
