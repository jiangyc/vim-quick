Plug 'flazz/vim-colorschemes'

call quick#util#plug#add_hook('vim-colorschemes', 'Hook_colorscheme')
fu! Hook_colorscheme()
    colorscheme monokain
endf
