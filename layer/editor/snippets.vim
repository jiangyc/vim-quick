
if quick#util#plug#has_plug('coc.nvim') && (has('python') || has('python3'))
    call insert(g:coc_global_extensions, 'coc-snippets')
    Plug 'honza/vim-snippets'
endif
