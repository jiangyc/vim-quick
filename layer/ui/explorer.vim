
" 1、coc-explorer
" 2、defx

if quick#util#plug#has_plug('coc.nvim')
    call insert(g:coc_global_extensions, 'coc-explorer')
    call quick#util#plug#add_hook('coc.nvim', 'Hook_explorer')
endif

fu! QuickToggleExplorer() 
    CocCommand explorer
endf

" hook
fu! Hook_explorer()
    command! -nargs=0 QuickToggleExplorer call QuickToggleExplorer()
    nnoremap <SPACE>vf :QuickToggleExplorer<CR>
endf
