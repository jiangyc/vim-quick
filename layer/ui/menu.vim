
Plug 'skywind3000/vim-quickui'

call quick#util#plug#add_hook('vim-quickui', 'Hook_menu')
fu! Hook_menu()
    let g:quickui_show_tip = 1
    let g:quickui_border_style = 2

    call quick#view#menu#init()    
    nnoremap <silent><space><space> :call quickui#menu#open()<cr>
endf
