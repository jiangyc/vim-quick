
" 1、coc-terminal

if quick#util#plug#has_plug('coc.nvim')
    call insert(g:coc_global_extensions, 'coc-terminal')
    call quick#util#plug#add_hook('coc.nvim', 'Hook_terminal')
endif

fu! QuickToggleTerminal()
    CocCommand terminal.Destroy
    CocCommand terminal.Toggle
endf

" hook
fu! Hook_terminal()
    command! -nargs=0 QuickToggleTerminal call QuickToggleTerminal()
    nnoremap <SPACE>vt :QuickToggleTerminal<CR>
endf
