
" 1、vim-fencview

Plug 'vimchina/vim-fencview'

fu! QuickToggleEncoding()
    FencView
endf

" hook
call quick#util#plug#add_hook('vim-fencview', 'Hook_fencview')
fu! Hook_fencview()
    command! -nargs=0 QuickToggleEncoding call QuickToggleEncoding()
    nnoremap <SPACE>ve :QuickToggleEncoding<CR>
endf
