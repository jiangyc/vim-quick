
if executable('ctags') || quick#util#plug#has_plug('coc.nvim')
    Plug 'liuchengxu/vista.vim'
    call quick#util#plug#add_hook('vista.vim', 'Hook_vista')
endif

fu! QuickToggleTags()
    Vista!!
endf

" Hook
fu! Hook_vista()
    command! -nargs=0 QuickToggleTags call QuickToggleTags()
    nnoremap <SPACE>va :QuickToggleTags<CR>

    let g:vista_executive_for = {}
    if g:quick_coc_enabled && quick#util#plug#has_plug('coc.nvim')
        for lsp in g:quick_coc_config['lsp']
            for lsp_syntax in lsp['syntax']
                let g:vista_executive_for[lsp_syntax] = 'coc'
            endfor
        endfor
    endif

    let g:vista_icon_indent = ["╰─▸ ", "├─▸ "]
endf
