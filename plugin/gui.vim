" 
" Gui相关配置
"
" ------
"
if !(has('gui') || executable('nvim-qt'))
    finish
endif

" 字体
set guifont=FiraCode\ NF:h11
if has('linux')
    if has('nvim')
        set guifont=FiraMono\ Nerd\ Font
    else 
        set guifont=FiraCode\ Nerd\ Font\ 11 
    endif
endif
" 行和列
" set lines=30
" set columns=110

" 取消工具栏
set guioptions-=T
" 取消菜单栏
" set guioptions-=m
